<?php get_header()?>
<div id="section-about">
    <div class="background-section">
        <div class="content font-17-24">
            <div class="headline">
                <h1 class="font-title-section"><span>our</span> Skills</h1>
<!--                <span>OUR MAIN SKILLS</span>-->
                <div class="content team">
                    <?php
                    if( get_field('skills', 'options') ){
                        while ( has_sub_field('skills', 'options') ) {
                            ?>
                            <div class="list-skill">
                                <div class="item">
                                    <div class="thumbnail overflow">
                                        <img src="<?php the_sub_field('logo')?>" class="animation-zoom" alt="">
                                    </div>
                                    <div class="description-skill">
                                        <h3><?php the_sub_field('name')?></h3>
                                    </div>
                                </div>
                            </div>
                            <?php
                        }
                    }
                    ?>
                </div>

                <ul class="block_aplication">
                    <h1 class="font-title-section"><span>our</span> Process</h1>
                    <br>
                    <li class="element arrow-left red">
                        <div class="block_element">
                            <div class="sup_element">
                                <div class="link">
                                    <i class="icons_lorem1">&nbsp;</i>
                                    <span class="text_element">Order</span>
                                </div>
                            </div>
                        </div>
                    </li>
                    <li class="element arrow-left white">
                        <div class="block_element">
                            <div class="sup_element">
                                <div class="link">
                                    <i class="icons_lorem2">&nbsp;</i>
                                    <span class="text_element">Quote</span>
                                </div>
                            </div>
                        </div>
                    </li>
                    <li class="element arrow-bottom blue">
                        <div class="block_element">
                            <div class="sup_element">
                                <div class="link">
                                    <i class="icons_lorem3">&nbsp;</i>
                                    <span class="text_element">Project specification</span>
                                </div>
                            </div>
                        </div>
                    </li>
                    <li class="element white">
                        <div class="block_element">
                            <div class="sup_element">
                                <div class="link">
                                    <i class="icons_lorem6">&nbsp;</i>
                                    <span class="text_element">Testing</span>
                                </div>
                            </div>
                        </div>
                    </li>
                    <li class="element arrow-right green">
                        <div class="block_element">
                            <div class="sup_element">
                                <div class="link">
                                    <i class="icons_lorem5">&nbsp;</i>
                                    <span class="text_element">Coding</span>
                                </div>
                            </div>
                        </div>
                    </li>
                    <li class="element arrow-right white">
                        <div class="block_element">
                            <div class="sup_element">
                                <div class="link">
                                    <i class="icons_lorem4">&nbsp;</i>
                                    <span class="text_element">Wireframes + DB design</span>
                                </div>
                            </div>
                        </div>
                    </li>
                    <li class="element yellow">
                        <div class="block_element">
                            <div class="sup_element">
                                <ul class="list_icons">
                                    <li class="block_icons">
                                        <div class="link">
                                            <i class="icons_lorem10">&nbsp;</i>
                                        </div>
                                    </li>
                                    <li class="block_icons">
                                        <div class="link">
                                            <i class="icons_lorem9">&nbsp;</i>
                                        </div>
                                    </li>
                                    <li class="block_icons">
                                        <div class="link">
                                            <i class="icons_lorem8">&nbsp;</i>
                                        </div>
                                    </li>
                                    <li class="block_icons">
                                        <div class="link">
                                            <i class="icons_lorem7">&nbsp;</i>
                                        </div>
                                    </li>
                                </ul>
                                <div class="link">
                                    <span class="text_element">Release</span>
                                </div>
                            </div>
                        </div>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>
<?php get_footer()?>

