<?php get_header();?>
<div id="section-single">
    <div class="content">
        <div class="title-post">
            <h2><?php the_title()?></h2>
        </div>
        <?php
            if( get_field("gallary")) {
                ?>
                <div class="swiper-container">
                    <div class="swiper-wrapper">
                        <?php
                        if( get_field('gallary') ) {
                            ?>

                            <?php
                            while ( has_sub_field('gallary') ) {
                                ?>
                                <div class="swiper-slide feature-image" style="background-image: url('<?php the_sub_field('image')?>')">

                                </div>
                                <?php
                            }
                            ?>
                            <?php
                        }
                        ?>
                    </div>
                    <!-- Add Pagination -->
                    <div class="swiper-pagination"></div>
                    <!-- Add Arrows -->
                    <div class="swiper-button-next"></div>
                    <div class="swiper-button-prev"></div>
                </div>

                <?php
            } elseif ( get_field("video_project")) {
                ?>
                <div class="">
                    <video autoplay="" loop="" style="width: 100%">
                        <source src="<?php echo get_field("video_project")?>" type="video/mp4">
                    </video>
                </div>
                <?php
            }
        ?>

        <div class="row-single">
            <div class="detail">
                <?php
                $prev_post = get_previous_post();
                $next_post = get_next_post();
                ?>
                <div class="pagination-post">
                    <div class="previous icon">
                        <a href="<?php echo get_permalink($prev_post->ID) ?>"><i class="fa fa-arrow-left" aria-hidden="true"></i></a>
                    </div>
                    <div class="icon padding"></div>
                    <div class="next icon">
                        <a href="<?php echo get_permalink($next_post->ID) ?>"><i class="fa fa-arrow-right" aria-hidden="true"></i></a>
                    </div>
                </div>
<!--                <div class="title">-->
<!--                    <h2>--><?php //the_title()?><!--</h2>-->
<!--                </div>-->
                <div class="content-detail">
                    <?php
                    if (have_posts()):
                        while (have_posts()) : the_post();
                            the_content();
                        endwhile;
                    endif;
                    ?>
                </div>

            </div>
            <div class="infomation">
                <div class="social-post">
                    <a href="https://www.facebook.com/sharer/sharer.php?u=<?php the_permalink()?>" target="_blank">Chia sẻ</a>
                </div>
                <div class="info">
                    <div class="row row-content">
                        <div class="col col-2">
                            <i class="fa fa-calendar-check-o" aria-hidden="true"></i>
                        </div>
                        <div class="col">
                            <b>Giá Bán:</b>
                            <p><?php the_field('gia_ban')?></p>
                        </div>
                    </div>
                </div>
                <div class="info">
                    <div class="row row-content">
                        <div class="col col-2">
                            <i class="fa fa-compass" aria-hidden="true"></i>
                        </div>
                        <div class="col">
                            <b>Hướng:</b><br>
                            <p><?php the_field('huong')?></p>
                        </div>
                    </div>
                </div>
                <div class="info">
                    <div class="row row-content">
                        <div class="col col-2">
                            <i class="fa fa-map-marker" aria-hidden="true"></i>
                        </div>
                        <div class="col">
                            <b>Khu Vực:</b><br>
                            <p><?php the_field('khu_vuc')?></p>
                            <b>Địa Chỉ: </b><br>
                            <p><?php the_field('dia_chi')?></p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="related-post">
            <h2>Có thể bạn quan tâm</h2>
            <div class="list-project">
                <?php
                $wpb_all_query = new WP_Query(array('post_type'=>'post', 'post_status'=>'publish','orderby'   => 'rand', 'posts_per_page'=>4, 'post__not_in' => array( $post->ID )) ); ?>
                <?php if ( $wpb_all_query->have_posts() ) : ?>
                    <ul>
                        <?php while ( $wpb_all_query->have_posts() ) : $wpb_all_query->the_post();
                            $src = wp_get_attachment_image_src(get_post_thumbnail_id($post->ID), 'full', false);
                        ?>
                            <div class="project-item font-16-24">
                                <a href="<?php the_permalink()?>">
                                    <div class="background-related" style="background-image: url('<?php echo $src[0]?>')">
                                    </div>
                                    <div class="short-detail">
                                        <h3 class="font-18-40"><?php echo get_the_title()?></h3>
                                        <p>Giá Bán: <?php the_field('gia_ban')?></p>
                                        <p>Diện Tích: <?php the_field('dien_tich')?></p>
                                    </div>
                                </a>
                            </div>
                        <?php endwhile; ?>
                    </ul>
                    <?php wp_reset_postdata(); ?>
                <?php endif; ?>
            </div>
        </div>
    </div>
</div>
<?php get_footer();?>
