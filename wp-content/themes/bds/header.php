<!DOCTYPE html>
<html>
<head>
    <title><?php wp_title('');?> <?php if(is_home()) {echo "Bất Động Sản Ven Sông Hàn";} else { echo "| Bất Động Sản Ven Sông Hàn";}?> </title>
    <meta charset="UTF-8">
    <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
    <link rel="shortcut icon" href="<?php echo get_template_directory_uri().'/assets/images/vsh.png'?>">

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/masonry/4.2.0/masonry.pkgd.js"></script>
    <script type='text/javascript' src='https://cdnjs.cloudflare.com/ajax/libs/Swiper/4.0.5/js/swiper.min.js'></script>

    <!--    font-->
    <link href="https://fonts.googleapis.com/css?family=Montserrat:300,400,900|Open+Sans" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Timmana" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Ubuntu" rel="stylesheet">
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" media='all'>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/Swiper/4.0.5/css/swiper.min.css">
</head>
<?php wp_head()?>
<body <?php body_class()?> >
<div class="bubblingG">
    <div class="middle">
        <span id="bubblingG_1">
	</span>
        <span id="bubblingG_2">
	</span>
        <span id="bubblingG_3">
	</span>
    </div>
</div>
<div class="wrapper">
    <header class="desktop">
        <div class="logo logo-header">
            <div>
                <a href="<?php echo home_url()?>">
                    <img src="<?php echo get_template_directory_uri().'/assets/images/vsh.png'?>" alt="">
                </a>
            </div>
        </div>
        <div class="nav-desktop active-desktop container">
            <nav class="nav-menu">
                <ul>
                    <?php
                    wp_nav_menu(array(
                        'theme_location' => 'header',
                        'container' => false,
                        'items_wrap' => '%3$s',
                        'fallback_cb' => 'alert_menu'
                    ));
                    ?>
                </ul>
            </nav>
        </div>
        <div class="social">
            <div>
                <span class="developer-copyrights ">
                    Copyright 2018 <a href="<?php echo home_url()?>" target="blank"><strong>Ven Sông Hàn</strong>.</a>
                </span>
            </div>
            <div class="social-item">
                <a href="http://jevelin.shufflehound.com" target="_blank" class="social-media-twitter">
                    <i class="fa fa-youtube-play" aria-hidden="true"></i>
                </a><a href="http://jevelin.shufflehound.com" target="_blank" class="social-media-facebook">
                    <i class="fa fa-facebook" aria-hidden="true"></i>
                </a><a href="http://jevelin.shufflehound.com" target="_blank" class="social-media-instagram">
                    <i class="fa fa-instagram" aria-hidden="true"></i>
                </a>
                <div class="sh-clear"></div>
            </div>
        </div>
    </header>
    <header class="mobile">
        <div class="container">
            <div class="row">
                <div class="col">
                    <a href="<?php echo home_url()?>">
                        <img src="<?php echo get_template_directory_uri().'/assets/images/vsh.png'?>" alt="">
                    </a>
                </div>

                <div class="col nav-hamburger">
                    <div id="nav-icon1">
                        <span></span>
                        <span></span>
                        <span></span>
                    </div>
                </div>
            </div>
        </div>
        <div class="dropdown-menu">
            <ul>
                <?php
                    wp_nav_menu(array(
                        'theme_location' => 'header',
                        'container' => false,
                        'items_wrap' => '%3$s',
                        'fallback_cb' => 'alert_menu'
                    ));
                ?>
            </ul>
        </div>
    </header>
    <div class="wrap">