<?php get_header()?>
<div id="section-about">
    <div class="background-section">
        <div class="content font-17-24">
            <span>OOPS!!!</span>
            <h1>ERROR 404</h1>
            <p>Sorry! The Page you are looking for can't be found</p><br>
            <a href="<?php echo home_url()?>" class="button-1">Back to home</a>
        </div>
    </div>
</div>
<?php get_footer()?>

