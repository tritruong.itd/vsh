<?php

include "functions/acf-option-page.php";

// ENQUEUE
function itd_scripts() {

    //Stylesheet

    wp_enqueue_style( 'itd-bootstrap-style', get_template_directory_uri().'/assets/libs/bootstrap.min.css' );
    wp_enqueue_style( 'itd-animate-style', get_template_directory_uri().'/assets/libs/animate.css' );
    wp_enqueue_style( 'itd-style', get_stylesheet_uri() );


    //Scripts

    wp_enqueue_script( 'itd-script', get_template_directory_uri().'/assets/js/itd-main.js' );
    wp_enqueue_script( 'itd-script-scroll', get_template_directory_uri().'/assets/js/AnimOnScroll.js' );
    wp_enqueue_script( 'itd-script-modernizr', get_template_directory_uri().'/assets/js/classie.js' );
    wp_enqueue_script( 'itd-script-classie', get_template_directory_uri().'/assets/js/modernizr.custom.js' );
    wp_enqueue_script( 'itd-script-pk', get_template_directory_uri().'/assets/js/imagesloaded.pk.min.js' );
    wp_enqueue_script( 'itd-script-isotope', get_template_directory_uri().'/assets/js/isotope.js' );

}

add_action( 'wp_enqueue_scripts', 'itd_scripts' );

// SEO

function itd_insert_fb_in_head() {

    global $post;
    if ( !is_singular()) //if it is not a post or a page
        return;
    echo '<meta property="og:title" content="' . get_the_title() . '"/>';
    echo '<meta property="og:type" content="article"/>';
    echo '<meta property="og:url" content="' . get_permalink() . '"/>';
    echo '<meta property="og:site_name" content="ITDragons"/>';
    if(!has_post_thumbnail( $post->ID ))
    { //the post does not have featured image, use a default image
        $default_image="http://example.com/image.jpg"; //replace this with a default image on your server or an image in your media library
        echo '<meta property="og:image" content="' . $default_image . '"/>';
    }
    else{
        $thumbnail_src = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'medium' );
        echo '<meta property="og:image" content="' . esc_attr( $thumbnail_src[0] ) . '"/>';
    }
    echo "
";
}

add_action( 'wp_head', 'itd_insert_fb_in_head', 5 );

//NAV

function itd_theme_nav_setup()
{
    register_nav_menus(array(
        'header' => '1ST MENU',
        'footer' => '2ND MENU'
    ));
}

add_action('after_setup_theme', 'itd_theme_nav_setup');

function itd_insert_meta_title() {
    echo '<meta property="og:title" content="' . get_the_title() . '"/>';
}

function itd_theme_setup()
{
    add_theme_support('post-thumbnails');
}
add_action( 'after_setup_theme', 'itd_theme_setup' );