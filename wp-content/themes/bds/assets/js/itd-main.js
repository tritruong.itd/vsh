$(document).ready(function(){

    var nav_item = $('.nav-menu li');
    var width_device = $(window).width();

    if(width_device > 768) {
        nav_item.hover(
            function(){
                $('.menu-separator').css({"background-color": "rgb(218, 36, 74)", "width": "100%"});
                $('.title').addClass('active-menu-text');
            },
            function() {
                $('.menu-separator').css({"background-color": "rgb(255, 255, 255)", "width": "22px"});
                $('.title').removeClass('active-menu-text');
            }
        )
    }


    $('#nav-icon1').click(function(){
        $(this).toggleClass('open');
        $('.dropdown-menu').slideToggle();
    });

    $('.porfolio-item').hover(
        function(){
            $(this).find('.bg').css('opacity',1);
            $(this).find('.detail_link').addClass('detail_link-hover');
            $(this).find('.content-item').css('border-radius','10px');
        },
        function () {
            $(this).find('.bg').css('opacity',0);
            $(this).find('.detail_link').removeClass('detail_link-hover');
            $(this).find('.content-item').css('border-radius',0);
        }
    );

    loadFilter();
    // callPopup();
    filter();
    swiper();

});

$(window).bind("load", function() {
    setTimeout(function () {
        $('.bubblingG').fadeOut();
        $('html').css('overflow','auto');
        setMasonryGrid();
    },1000);
});


function filter() {

    $('.filter-cate li a').click(function(){
        $('.filter-cate li a').removeClass('active-filter');
        $('.porfolio-item').hide();
        $(this).addClass('active-filter');
        var data = $(this).attr('data-slug'),
            class_data =  '.' + $(this).attr('data-slug');
        $('.filter-cate').attr('data-active-cat',data);

        $(class_data).show();
        setMasonryGrid();
    })
}

function loadFilter() {
    var url = window.location.href.split("#"),
        cat = '.'+url[1];
    $('.filter-cate li a').removeClass('active-filter');

    if(cat === ".undefined") {
        cat = '.all';
    }
    $('.porfolio-item').hide();
    if( $(cat).length) {
        $(cat).show();
    }


    $('.filter-cate li').each(function() {
        var data_fil = '.'+$(this).find('a').attr('data-slug');
        if(data_fil === cat) {
            $(this).find('a').addClass('active-filter');
        }
    });
    setMasonryGrid();
}

function setMasonryGrid() {
    $('.grid').masonry({
        itemSelector: '.grid-item',
        columnWidth: 1,
        percentPosition: true,
        isAnimated: true
    })
}

function callPopup(){
    $('.click-detail').click(function (e) {
        $('.bg-popup').fadeIn();
        $('.popup-detail').addClass('active-popup');
        swiper();
    });
    $('.close_popup').click(function (e) {
        $('.bg-popup').fadeOut();
        $('.popup-detail').removeClass('active-popup');
    })
}

function swiper() {
    var swiper = new Swiper('.swiper-container', {
        spaceBetween: 30,
        pagination: {
            el: '.swiper-pagination',
            clickable: true
        },
        navigation: {
            nextEl: '.swiper-button-next',
            prevEl: '.swiper-button-prev'
        }
    });
}



