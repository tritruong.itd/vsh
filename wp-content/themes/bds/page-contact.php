<?php get_header()?>
    <div id="section-contact">
        <div class="background-section">
            <div class="content font-17-24">
                <div class="headline">
                    <h1 class="font-title-section"><span>Liên Hệ</span></h1>

                    <div class="content about">
                        <div class="row">
                            <div class="col-left" >
                                <p>Địa Chỉ: <?php the_field('address','options')?></p>
                                <table>
                                    <tr>
                                        <td style="color: #8d8d8d">Liên Hệ :</td>
                                        <td><a href="tel:<?php the_field('phone','options')?>"><?php the_field('phone','options')?></a></td>
                                    </tr>
                                    <tr>
                                        <td></td>
                                        <td><a href="tel:<?php the_field('mail','options')?>"><?php the_field('mail','options')?></a></td>
                                    </tr>
                                </table>
                                <p>Website: <a href="http://bdsvensonghan.com">www.bdsvensonghan.com</a></p>
                                <iframe src="<?php the_field('map','options')?>" width="100%" height="auto" frameborder="0" style="border:0" allowfullscreen></iframe>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php get_footer()?>