<div id="section-homepage">
    <div class="background-section">
        <div class="content font-17-24">
            <div class="headline">
                <h1 class="font-title-section"><span>ĐẤT BÁN</span></h1>
                <div class="tab-mansory font-13-17">
                    <ul class="filter-cate" data-active-cat="all">
                        <li>
                            <a href="#all" class="button-1 active-filter" data-slug="all" >Tất Cả</a>
                        </li>
                        <?php $categories = get_categories();
                            foreach ($categories as $cate) {
                                ?>
                                <li>
                                    <a href="#<?php echo $cate->slug?>" class="button-1" data-slug="<?php echo $cate->slug?>"><?php echo $cate->name?></a>
                                </li>
                                <?php
                            }
                        ?>
                    </ul>
                </div>
            </div>
            <?php
                $post_objects = get_field('posts','options');
                if( $post_objects ): ?>
                    <ul class="content grid effect-2" id="grid">
                        <?php foreach( $post_objects as $post):  ?>
                            <?php
                                setup_postdata($post);
                                $src = wp_get_attachment_image_src(get_post_thumbnail_id($post->ID), 'full', false);
                                $category = get_the_category($post->ID);
                            ?>
                            <li class="zoomIn porfolio-item grid-item all <?php foreach ($category as $cat) { echo $cat->slug . ' '; }?>" >
                                <div class="content-item ">
                                    <div class="overflow">
                                        <img class="thumbnail animation-zoom" src="<?php echo $src[0]?>" alt="" >
                                        <a href="javascript:void(0)" class="bg"></a>
                                        <div class="detail_link">
                                            <a href="<?php echo get_permalink()?>">Chi tiết <img src="<?php echo get_template_directory_uri()?>/assets/images/icon-right.png"></a>
                                        </div>
                                    </div>
                                    <div class="descriptions">
                                        <h3><?php the_title()?></h3>
                                        <div class="detail">
                                            <table>
                                                <tr>
                                                    <td class="left">Diện Tích:</td>
                                                    <td><?php the_field('dien_tich')?></td>
                                                </tr>
                                                <tr>
                                                    <td class="left">Hướng:</td>
                                                    <td><?php the_field('huong')?></td>
                                                </tr>
                                                <tr>
                                                    <td class="left">Địa chỉ:</td>
                                                    <td><?php the_field('dia_chi')?></td>
                                                </tr>
                                                <tr>
                                                    <td class="left">Khu Vực:</td>
                                                    <td><?php the_field('khu_vuc')?></td>
                                                </tr>
                                                <tr>
                                                    <td class="left">Giá Bán:</td>
                                                    <td><?php the_field('gia_ban')?></td>
                                                </tr>
                                                <tr>
                                                    <td class="left">Mặt Đường:</td>
                                                    <td><?php the_field('dt_mat_duong')?></td>
                                                </tr>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </li>
                        <?php endforeach; ?>
                    </ul>
                    <?php wp_reset_postdata(); ?>
                <?php endif;
            ?>
        </div>
    </div>
</div>