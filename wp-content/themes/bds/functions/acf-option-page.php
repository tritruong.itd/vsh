<?php

if ( function_exists( 'acf_add_options_page' ) ) {

    acf_add_options_page( array(
        'page_title'  => 'Trang Chủ',
        'menu_title'  => 'Trang Chủ',
        'menu_slug'   => 'home-page'
    ) );

    acf_add_options_page( array(
        'page_title'  => 'Thông Tin Công Ty',
        'menu_title'  => 'Thông Tin Công Ty',
        'menu_slug'   => 'about-page'
    ) );



    acf_add_options_page( array(
        'page_title'  => 'Liên Hệ',
        'menu_title'  => 'Liên Hệ',
        'menu_slug'   => 'contact-page'
    ) );

}