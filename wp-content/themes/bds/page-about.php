<?php get_header()?>
<div id="section-about">
    <div class="background-section">
        <div class="content font-17-24">
            <div class="headline">
                <h1 class="font-title-section"><span>Giới Thiệu Về Công Ty</span></h1>
<!--                <span>WE CREATE AWESOME STUFF</span>-->
                <div class="content about">
                    <?php the_field('about','options')?>
                </div>
            </div>
            <div class="headline">
                <h1 class="font-title-section"><span>Thành Viên</span></h1>
<!--                <span>WHO WE ARE</span>-->
                <div class="content team">
<!--                    <div class="font-16-24 description-team">-->
<!--                        --><?php //the_field('team_description','options')?>
<!--                    </div>-->
                    <?php

                    if( get_field('team', 'options') ){
                        while ( has_sub_field('team', 'options') ) {
                            ?>
                            <div class="list-team">
                                <div class="item">
                                    <div class="thumbnail overflow">
                                        <img src="<?php the_sub_field('avatar')?>"
                                             class="" alt="">
                                    </div>
                                    <div class="description">
                                        <h1><?php the_sub_field('name')?></h1>
                                        <table>
                                            <tr>
                                                <td style="color: #8d8d8d"><h2>Liên Hệ :</h2></td>
                                                <td><a href="tel:<?php the_sub_field('phone1')?>"><h2><?php the_sub_field('phone1')?></h2></a></td>
                                            </tr>
                                            <tr>
                                                <td></td>
                                                <td><a href="tel:<?php the_sub_field('phone2')?>"><h2><?php the_sub_field('phone2')?></h2></a></td>
                                            </tr>
                                        </table>
                                    </div>
                                </div>
                            </div>
                            <?php
                        }
                    }
                    ?>
                </div>
            </div>
        </div>
    </div>
</div>
<?php get_footer()?>

